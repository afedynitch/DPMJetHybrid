#!/bin/csh
# This is a running example using sPHENIX's gcc 8.3 suite
# when you log on the eic node.

setenv CERN_ROOT $EICDIRECTORY/gcc83lib
setenv LHAPDF5 $EICDIRECTORY/gcc83lib
setenv LD_LIBRARY_PATH ${LHAPDF5}
source /cvmfs/sphenix.sdcc.bnl.gov/gcc-8.3/opt/sphenix/core/gcc/8.3.0.1-0a5ad/x86_64-centos7/setup.csh
source /cvmfs/sphenix.sdcc.bnl.gov/gcc-8.3/opt/sphenix/core/binutils/2.30-e5b21/x86_64-centos7/setup.csh

## Use the following to prepare before running
# cp $FLUPRO/nuclear.bin .
# cp $EICDIRECTORY/PACKAGES/DPMJetHybrid/read_ep .
# cp $EICDIRECTORY/PACKAGES/DPMJetHybrid/ep.inp .
# mkdir outForPythiaMode
# ln -s $EICDIRECTORY/PACKAGES/DPMJetHybrid/PyQM

$EICDIRECTORY/PACKAGES/dpmjetHybrid < ep.inp > ep.log 
