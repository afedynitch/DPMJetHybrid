###Set up some paths
TOP = $(PWD)
###################make file for DPMJET with pythia################
EXE = dpmjetHybrid
MAIN = user-eA3.0-5

###################compiler - options#######################
#32 bit
#CXX      = g77
#CXXFLAGS = -c -g -Wall -m32 -fno-inline -fno-automatic
#LD = $(CXX) -g -m32

#64 bit
#CXX      = gfortran
#gcc 4.6.3 is used because of the fluka version.
#The 64 bit fluka lib can be compiled with gcc versions
#higher than 4.6, so our eic installed 64bit fluka
#is compiled with this version.
#To be compatible with that compiled fluka lib
#this dpmjet code has to be done with the same gcc version.
#When everything has been done, the gcc 4.6.3 lib has to
#be included in LD_LIBRARY_PATH. On eic, add
#/opt/gcc/4.6.3/lib64/ to to your LD_LIBRARY_PATH in 
#your .cshrc

# KK: source 8.3 path instead:
# setenv CERN_ROOT $EICDIRECTORY/gcc83lib
# setenv LHAPDF5 $EICDIRECTORY/gcc83lib
# setenv LD_LIBRARY_PATH ${LHAPDF5}
# source /cvmfs/sphenix.sdcc.bnl.gov/gcc-8.3/opt/sphenix/core/gcc/8.3.0.1-0a5ad/x86_64-centos7/setup.csh
# source /cvmfs/sphenix.sdcc.bnl.gov/gcc-8.3/opt/sphenix/core/binutils/2.30-e5b21/x86_64-centos7/setup.csh
#CXX	= /opt/gcc/4.6.3/bin/gfortran
CXXFLAGS = -c -g -Wall -m64 -fno-inline -fno-automatic
LD = $(CXX) -g -m64 -lgfortran
LD += -static-libgfortran

##################directories###############################
PYTHIA   = PYTHIA-6.4.13
PROGRAM  = dpmjet
PYQM = PyQM
# KK don't use
# RADGEN = radgen

#to use the eic updated PYTHIA source
#uncomment the following two lines and comment out
#the pythia and radgen directory in current source list
#PYTHIASRC = /afs/rhic/eic/PACKAGES/PYTHIA-PP/PYTHIA-6.4.28/
#RADGENSRC = linkToRadgen/

#src, obj directory definition for
#pythia, pyqm and dpmjet radgen
PYTHIASRC = $(TOP)/$(PYTHIA)/
PYTHIAOBJ = $(TOP)/obj/$(PYTHIA)/
PYQMSRC = $(TOP)/$(PYQM)/
PYQMOBJ = $(TOP)/obj/$(PYQM)/
PROGRAMSRC = $(TOP)/src/
PROGRAMOBJ = $(TOP)/obj/$(PROGRAM)/
# KK don't use
# RADGENSRC = $(TOP)/$(RADGEN)/
# RADGENOBJ = $(TOP)/obj/$(RADGEN)/


MAINOBJ = $(TOP)/obj/$(MAIN)
MAINSRC = $(TOP)/$(MAIN)

#####################libraries######################################
#change the following library path according to the setting in 
#your environment
#FLUKA = /afs/rhic/eic/PACKAGES/fluka-32/
#LIB1 = -L/cern/pro/lib -lmathlib -lkernlib -lpacklib_noshift -ldl -lm 
#LIB2 = -L$(FLUKA) -lflukahp
#LIB3 = -L/afs/rhic.bnl.gov/eic/lib32 -lLHAPDF 

# FLUKA = /afs/rhic/eic/PACKAGES/fluka-64/
# LIB1 = -L/cern64/pro/lib -lmathlib -lkernlib -lpacklib_noshift -ldl -lm 
# LIB2 = -L$(FLUKA) -lflukahp
# LIB3 = -L/afs/rhic/eic/lib -lLHAPDF 

# KK BNL:
FLUKA = $(FLUPRO)/
LIB1 = -L$(CERN_ROOT) -lmathlib -lkernlib -lpacklib -ldl -lm 
LIB2 = -L$(FLUKA) -lflukahp
LIB3 = -L$(LHAPDF5) -lLHAPDF 

#####################include directories###########################
INCLUDE = $(TOP)/include
#change the following fluka path according to the setting in 
#your environment
FLUINC = $(FLUKA)flukapro

##This fpe.o module can not be used when PYTHIA runs
##since some pythia routines like pysppa which orders a
##initial shower will introduce some +-INF value but not
##allowed by this routine.
#TRAP = fpe.o


##################### helper to create subdirs###########################
dir_guard=@mkdir -p $(@D)


######################dependence rules#####################################
all:	$(EXE)

#pythia library
#get the stripped off file name list by patsubst
pythia_objects_ := $(patsubst %.f,%.o,$(notdir $(wildcard $(PYTHIASRC)*.f)))
#get the object file list which prefixed with the obj path directory
pythia_objects := $(patsubst %,$(PYTHIAOBJ)/%,$(pythia_objects_))

obj/libpythia6.a : $(pythia_objects)
	$(dir_guard)
	cd $(TOP)/obj; ar rcf libpythia6.a $(pythia_objects); ranlib libpythia6.a

#build the compiling rule for the source files
$(PYTHIAOBJ)/%.o : $(PYTHIASRC)%.f
	$(dir_guard)
	$(CXX) -c $(CXXFLAGS) -I$(INCLUDE) $< -o $@

# #radgen library
# KK : Use existing library instead
# radgen_objects_ := $(patsubst %.f,%.o,$(notdir $(wildcard $(RADGENSRC)*.f)))
# radgen_objects := $(patsubst %,$(RADGENOBJ)/%,$(radgen_objects_))
RADGENODIR = $(EICDIRECTORY)/PACKAGES/PYTHIA-RAD-CORR/build/CMakeFiles/pythia6-eicmod-objects.dir/src/radcorr
radgen_objects := $(RADGENODIR)/*.o


obj/libradgen.a : 
	$(dir_guard)
	cd $(TOP)/obj; ar rcf libradgen.a $(radgen_objects); ranlib libradgen.a

# $(RADGENOBJ)/%.o : $(RADGENSRC)/%.f
# 	$(CXX)  $(CXXFLAGS) -I$(INCLUDE) $< -o $@

#pyqm library
pyqm_objects_ := $(patsubst %.f,%.o,$(notdir $(wildcard $(PYQMSRC)*.f)))
pyqm_objects := $(patsubst %,$(PYQMOBJ)/%,$(pyqm_objects_))

obj/libpyqm.a : $(pyqm_objects)
	$(dir_guard)
	cd $(TOP)/obj; ar rcf libpyqm.a $(pyqm_objects); ranlib libpyqm.a

$(PYQMOBJ)/%.o : $(PYQMSRC)%.f
	$(dir_guard)
	$(CXX)  $(CXXFLAGS) -I$(INCLUDE) $< -o $@

#dpmjet library
dpmjet_objects_ := $(patsubst %.f,%.o,$(notdir $(wildcard $(PROGRAMSRC)*.f)))
dpmjet_objects := $(patsubst %,$(PROGRAMOBJ)/%,$(dpmjet_objects_))

obj/libdpmjet.a : $(dpmjet_objects)
	cd $(TOP)/obj; ar rcf libdpmjet.a $(dpmjet_objects); ranlib libdpmjet.a

$(PROGRAMOBJ)/%.o : $(PROGRAMSRC)%.f
	$(dir_guard)
	$(CXX)  $(CXXFLAGS) -I$(INCLUDE) -I$(FLUINC) $< -o $@

#main program object
$(MAINOBJ).o: $(MAINSRC).f
	$(dir_guard)
	cd $(TOP); $(CXX) $(CXXFLAGS) -I$(INCLUDE) -I$(FLUINC) $(MAIN).f  -o $@


#The linking sequence must be dpmjet, pyqm, pythia6, radge
#Since there is a dependence relationship
#dpmjet needs routines from pythia6, pythia6 needs routines from radge
#The fundamental one always comes later!!!!
$(EXE): $(MAINOBJ).o obj/libdpmjet.a obj/libpythia6.a obj/libradgen.a obj/libpyqm.a
	cd $(TOP); $(LD) -I$(INCLUDE) -I$(FLUINC) -o $(EXE) $(MAINOBJ).o \
			 -Lobj/ -ldpmjet -lpyqm -lpythia6 -lradgen \
			$(LIB1) $(LIB2) $(LIB3) 

#########################clean up utility##############################
clean:   
	rm -f $(EXE) core.* $(MAINOBJ).o $(PYTHIAOBJ)/*.o $(PROGRAMOBJ)/*.o $(PYQMOBJ)/*.o $(RADGENOBJ)/*.o obj/*.o obj/*.a

