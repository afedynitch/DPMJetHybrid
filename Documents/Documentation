This is a documentation about the usage of runcards and output file
structure for this hybrid Monte Carlo Package.


Runcard:
=========================================================================
When running this hybrid model, we will come across two runcards:
- ep.inp or eA.inp is the major runcard coordinating the whole running
of this program. 
- read_ep or read_eA is responsible for the parameters running pythia
model.

For the major runcard ep.inp or eA.inp:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Please follow the format: A10,6E10.0,A8, otherwise your runcard can not
be recognised by the program!!!
which means the card format should follow the structure below:

|123456789|123456789|123456789|123456789|123456789|123456789|123456789|1234567
cardname  what(1)   what(2)   what(3)   what(4)   what(5)   what(6)   sdum

Definitions of projectile particles:
use control card:PROJPAR
what(1..6)	no use
sdum		lepton code word
			only 4 types of lepton are allowed to use if use PYTHIA model
			ELECTRON
			POSITRON
			MUON+
			MUON-

Definitions of target particles:
use control card:TARPAR
what(1) =	mass number
what(2) =	charge number
what(3..6)	no use
sdum		no use

Definitions of beam energy:
(for PYTHIA mode suggested to use only MOMENTUM or ENERGY card)
use control card: MOMENTUM
what(1) =	lepton beam momentum
what(2) =	target beam momentum

Definitions of kinematics cut:
use control card: L-TAG
what(1)	=	Minimum in y
what(2)	=	Maximum in y
what(3)	=	Minimum in Q2
what(4)	=	Maximum in Q2

Choose the model to run:
use control card: MODEL
what(1..6)	no use
sdum		model name
			PYTHIA		for pythia model
			PHOJET		for phojet model

Specify pythia input parameter list:
use control card: PY-INPUT
what(1..6)	no use
sdum		list name
			read_ep for ep
			read_eA for eA

Specify the number of events to generate:
use control card: START
what(1)		number of events
what(2..6)	no use
sdum		no use

some comments:
The usage of these control should be constrained in the above options.
It is advised that when running pythia model, use this input file to
define your beam, energy, kinematic range, event number and pythia
parameter list only, and keep the other runcards as it is.

If you wanna run this program with PHOJET for DIS events instead of
PYTHIA, please select PHOJET in the control card of MODEL, and follow
the rules of runcards mentioned in the DPMJET/PHOJET manual. The only
difference is for the beam energy set, it is better to stick to the
above definition of MOMENTUM card. And the output file needs to be
specified by OUTPUT card:
control card:	OUTPUT
what(1)		unit of output file (.e.g. 92)
what(2..6)	no use
sdum		no use
Then the output data file will be named as fort.92

(It is highly recommended to run this program with PYTHIA model when
beam energy is very low. The nucleus model with phojet doesn't accept
c.m.s energy of gamma*p lower than 3.5 GeV.)


read_ep/read_eA input card:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
This part is the parameters to be used in the pythia model.

line 1:		output file name should be wrapped within the quotes
if a symbol like "/" has to be used for specification of a certain
path.

line 2: kinematics cut used for radiate correction module, of no use 
in the current version.

line 3: model parameters used for radiate correction module, of no use 
in the current version.

line 4: switch for the radiate correction, needs to be put on 0 currently,
becauase this part is not done in our version.

line 5: GVMD model selection, should be put to 1 in the current version.

line 6: number of nucleons and charge for the target used in radgen
routines, of no use in the current version.

line 7: switch for the call of EPS09 nuclear PDF, 1:on, 0:off.

line 8: order and error set of this EPS09 nuclear PDF, x*100+y
		x=1 for LO
		  2	for NLO
		y for certain error set

line 9:	switch for the call of salgado&wiedemann energy loss effect
		1:on, 0:off.

line 10:qhat parameter used in salgado&wiedemann model.

line 11-end: PYTHIA parameters, please refer to the pythia manual.




Output:
=========================================================================
For PYTHIA model and PHOJET model we use different structure of the event
wise output data, but the same structure for the track wise data, specific
event wise data explanation can be found on the eic wiki web page:
https://wiki.bnl.gov/eic/index.php/PYTHIA
https://wiki.bnl.gov/eic/index.php/DPMJet

The particle wise varaible is the same as the above DPMJet page says.

One thing might be a little bit tricky is about the final state particles
in eA running mode.
In ep mode, all the final state particles are labeled with ISTHKK=1.
However in eA mode, the final state particles is made of 2 parts:
 - ISTHKK=1, from the hard interaction. 
 - ISTHKK=-1 and ISTHKK=1001 from the nuclear evaporation/absorption/heavy nuclear 
fragments.

If you want to check the initial nuclear configuration
 - use ISTHKK=14
 - to find the spectator nucleons in the nucleus target, ISTHKK=18 
 - to find the nucleons involved in the following evaporation process. 
 - ISTHKK=12 gives the nucleon involved in the hard interaction.


